FROM golang

ADD . /go/src/gitlab.com/haibin/ci-go/ci-go

RUN go install gitlab.com/haibin/ci-go/ci-go

ENTRYPOINT /go/bin/ci-go

EXPOSE 8080
